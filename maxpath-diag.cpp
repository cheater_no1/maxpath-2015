/*
PROB: given a martix F(1..N, 1..N) find a path with max sum of cells
      going from (1,1) to (N,N) and moving only (x,y) -> (x+1,y) or (x,y) -> (x,y+1)
SOLUTION: dynamic programming + cutting out the cells too far from the diagonal
COMPLEXITY: O(N*sqrt(N)) time and O(N*sqrt(N)) memory
AUTHOR: Petar Ivanov <peter.ivanov89@gmail.com>
COMMENT: the width can be chosen better
*/

#include <iostream>
#include <cstdio>
#include <vector>
#include <assert.h>

#include "rng.h"
using namespace std;

const int MAX_N = 10000 + 2;

int N, B, P;
int width;
vector<ll> dp[MAX_N];

ll solve() {
  for (int i=1; i <= N; i++)
    for (int j=max(1, i-width), shift=1; shift < 2*width && j <= N; shift++, j++)
      if (j >= 1)
        dp[i][shift] = F(i, j, B, P) + max(dp[i][shift-1], dp[i-1][shift+1]);

  return dp[N][1+width];
}

void print_path(int x, int y, int shift) {
  if (x>1 || y>1) {
    if (dp[x][shift] == F(x, y, B, P) + dp[x][shift-1])
      print_path(x, y-1, shift-1);
    else
      print_path(x-1, y, shift+1);
  }
  cout << x << " " << y << endl;
}

int main() {
  cin >> N >> B >> P;
  width = min(N, 2000000 / N) / 70;
  for(int i=0; i<=N; i++)
    dp[i] = vector<ll>(1+2*width, 0);
  cout << solve() << endl;
  print_path(N, N, 1+width);
  print_memory("diag            ");
  cerr << " width = " << width << endl;
  return 0;
}
