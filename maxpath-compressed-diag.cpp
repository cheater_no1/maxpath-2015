/*
PROB: given a martix F(1..N, 1..N) find a path with max sum of cells
      going from (1,1) to (N,N) and moving only (x,y) -> (x+1,y) or (x,y) -> (x,y+1)
SOLUTION: dynamic programming + cutting out the cells too far from the diagonal
COMPLEXITY: O(N*sqrt(N)) time and O(N*sqrt(N)) memory and storing only 1bit per cell
AUTHOR: Petar Ivanov <peter.ivanov89@gmail.com>
INFO: buggy
*/

#include <iostream>
#include <cstdio>
#include <vector>
#include <assert.h>

#include "rng.h"
using namespace std;

const int MAX_N = 10000 + 2;
const int DIST  = 200;

int N, B, P;
ll dp[2][MAX_N];
vector<bool> path[MAX_N];

ll solve() {
  for (int i=1; i <= N; i++)
    for (int j=i-DIST, shift=1; shift < 2*DIST && j <= N; shift++, j++)
      if (j >= 1) {
        if (dp[(i-1)&1][shift+1] > dp[i&1][shift-1]) {
          dp[i&1][shift] = F(i, j, B, P) + dp[(i-1)&1][shift+1];
          path[i][shift] = 0;
        } else {
          dp[i&1][shift] = F(i, j, B, P) + dp[i&1][shift-1];
          path[i][shift] = 1;
        }
      }

  return dp[N&1][1+DIST];
}

void print_path(int x, int y, int shift) {
  if (x>1 || y>1) {
    if (path[x][shift] == 0)
      print_path(x-1, y, shift+1);
    else
      print_path(x, y-1, shift-1);
  }
  cout << x << " " << y << endl;
}

int main() {
  cin >> N >> B >> P;
  for(int i=0; i<=N; i++)
    path[i].resize(1+2*DIST);
  cout << solve() << endl;
  print_path(N, N, 1+DIST);
  print_memory("compressed-diag ");
  return 0;
}
