/*
PROB: given a martix F(1..N, 1..N) find a path with max sum of cells
      going from (1,1) to (N,N) and moving only (x,y) -> (x+1,y) or (x,y) -> (x,y+1)
SOLUTION: dynamic programming
COMPLEXITY: O(N^2) time and O(N^2) memory
AUTHOR: Petar Ivanov <peter.ivanov89@gmail.com>
*/

#include <iostream>
#include <cstdio>
#include <vector>
#include <assert.h>

#include "rng.h"
using namespace std;

const int MAX_N = 10000 + 2;

int N, B, P;
vector<ll> dp[MAX_N];

ll solve() {
  for (int i=1; i <= N; i++)
    for (int j=1; j <= N; j++)
      dp[i][j] = F(i, j, B, P) + max(dp[i-1][j], dp[i][j-1]);

  return dp[N][N];
}

// for debug only
void print_matrix() {
  fprintf(stderr, "matrix:\n");
  for(int i=1; i <= N; i++) {
    for(int j=1; j <= N; j++)
      cerr << F(i, j, B, P) << " ";
    cerr << endl;
  }
}

void print_path(int x, int y) {
  if (x>1 || y>1) {
    if (dp[x][y] == F(x, y, B, P) + dp[x-1][y] && x>1)
      print_path(x-1, y);
    else
      print_path(x, y-1);
  }
  cout << x << " "  << y << endl;
}

int main() {
  cin >> N >> B >> P;
  for(int i=0; i<=N; i++)
    dp[i] = vector<ll>(N+1, 0);
  //print_matrix();
  cout << solve() << endl;
  print_path(N, N);
  print_memory("dummy           ");
  return 0;
}
