SHELL := /bin/bash
all: $(ALL)

%: %.cpp rng.h
	g++ -Wfatal-errors -std=c++11 -Wall -O2 -o $@ $<

all: maxpath-dummy maxpath-compressed-dummy maxpath-slow maxpath-diag maxpath-compressed-diag maxpath-cut maxpath-cut-diag maxpath-pesho maxpath-iskren checker
	@mkdir -p tests
	@cat gen.in | while read line; do \
	  id=$${line%% *}; \
	  line=$${line#* }; \
	  echo ${id}; \
	  inp=tests/maxpath.$${id}.in; \
	  GEN=gen; \
	  >&2 echo $$inp; \
	  >&2 cat $$inp; \
	  echo $$line > $$inp; \
 	  ./maxpath-dummy < $$inp > $${inp%.in}.sol; \
 	  #./maxpath-compressed-dummy < $$inp > $${inp%.in}.cdum; \
 	  #./maxpath-slow < $$inp > $${inp%.in}.slow; \
	  #./maxpath-diag < $$inp > $${inp%.in}.diag; \
	  #./maxpath-compressed-diag < $$inp > $${inp%.in}.cdiag; \
	  #./maxpath-cut < $$inp > $${inp%.in}.cut; \
	  #./maxpath-cut-diag < $$inp > $${inp%.in}.diagcut; \
	  ./maxpath-pesho < $$inp > $${inp%.in}.pes; \
	  ./maxpath-iskren < $$inp > $${inp%.in}.isk; \
	  ./checker $$inp $${inp%.in}.isk $${inp%.in}.sol; \
	  #./checker $$inp $${inp%.in}.pes $${inp%.in}.sol; \
	  >&2 echo ""; \
	done
