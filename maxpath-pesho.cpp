/*
PROB: given a martix F(1..N, 1..N) find a path with max sum of cells
      going from (1,1) to (N,N) and moving only (x,y) -> (x+1,y) or (x,y) -> (x,y+1)
SOLUTION: divide and conquere, dynamic programming
COMPLEXITY: O(N^2) time and O(N) memory
AUTHOR: Petar Ivanov <peter.ivanov89@gmail.com>
*/

#include <iostream>
#include <memory.h>
#include <assert.h>
#include "rng.h"
using namespace std;

const int MAX_N = 10000 + 2;

int N, B, P;
int R[MAX_N];  // R[r]=c iff some max path passes through (r,c) but not through (r,c+1)

ll dp1[2][MAX_N];  // going top-down: from r1 to m
ll dp2[2][MAX_N];  // going bottom-up: from r2 to m+1

// rf = row from, ct = column to
// calculate the optimal path sum from rf, cf to the row rt
// works both downwards and upwards
void solve(int rf, int rt, int cf, int ct, ll dp[][MAX_N]) {
  // direction of moving from rf to rt and from cf to ct
  int d = (rf <= rt && cf <= ct) ? +1 : -1;
  for (int j=cf; d*j <= d*ct; j+=d) {
    dp[(rf^1)&1][j] = 0;
  }

  for (int i=rf; d*i <= d*rt; i+=d) {
    dp[i&1][cf-d] = 0;
    for (int j=cf; d*j <= d*ct; j+=d)
      dp[i&1][j] = F(i, j, B, P) + max(dp[(i-d)&1][j], dp[i&1][j-d]);
  }
}

// returns the last optimal column number
int find_last_max(int rr1, int rr2, int c1, int c2) {
  ll cc = c1;
  for (int j=c1+1; j<=c2; j++)
    if (dp1[rr1&1][j] + dp2[rr2&1][j] > dp1[rr1&1][cc] + dp2[rr2&1][cc])
      cc = j;
  return cc;
}

// return the sum of cell of a max path going from (r1,c1) to (r2,c2)
// and update all R[r1..r2]
// recursion depth: O(logN)
ll divide_and_conquer(int r1, int r2, int c1, int c2) {
  if (r1 == r2) {
    ll ans = 0;
    if (c2 < c1) swap(c1, c2);
    for (int j=c1; j <= c2; j++)
      ans += F(r1, j, B, P);
    R[r1] = c2;
    return ans;
  }

  int rr = (r1 + r2) / 2;
  solve(r1,   rr, c1, c2, dp1);
  solve(r2, rr+1, c2, c1, dp2);
  int cc = find_last_max(rr, rr+1, c1, c2);

  return divide_and_conquer(  r1, rr, c1, cc)
       + divide_and_conquer(rr+1, r2, cc, c2);
}

void print_path() {
  for (int i=1, j=1; i<=N; i++, j--)
    for (; j<=R[i]; j++)
      cout << i << " " << j << endl;
}

int main() {
  cin >> N >> B >> P;
  cout << divide_and_conquer(1, N, 1, N) << endl;
  print_path();
  print_memory("pesho           ");
  return 0;
}
