#include <iostream>
#include <cstdio>
#include <assert.h>
#include <algorithm>
#include "rng.h"
using namespace std;

#ifndef NDEBUG
#   define ASSERT(condition, message) \
    do { \
        if (! (condition)) { \
            std::cerr << "Assertion `" #condition "` failed in " << __FILE__ \
                      << " line " << __LINE__ << ": " << message << std::endl; \
            std::exit(EXIT_FAILURE); \
        } \
    } while (false)
#else
#   define ASSERT(condition, message) do { } while (false)
#endif

const int MIN = 2;
const int MAX = 10000;

int N, B, P;

int gcd(int a, int b) {
  return b == 0 ? a : gcd(b, a%b);
}

bool check_solution(FILE* f, ll& len, bool print) {
  int x, y, prevx=0, prevy=1, max_diag_offset=0;
  len = 0;
  for(int i=1; i<=2*N-1; i++) {
    if (fscanf(f, "%d %d", &x, &y) != 2) {
      if (print) printf("0\nCannot read the %d-th pair of coordinates.\n", i);
      return 0;
    }
    if (x<1 || x>N || y<1 || y>N) {
      if (print) printf("0\nThe %d-th pair of coordinated (%d, %d) out of range [1, %d].\n",
                        i, x, y, N);
      return 0;
    }
    if (!(x-prevx == 1 && y-prevy == 0) && !(x-prevx == 0 && y-prevy == 1)) {
      if (print) printf("0\nThe %d-th pair of coordinated (%d, %d) is not to the right "
                        "or to the bottom of the previous (%d, %d).\n", i, x, y, prevx, prevy);
      return 0;
    }
    len += F(x, y, B, P);
    max_diag_offset = max(max_diag_offset, abs(x-y));
    prevx = x, prevy = y;
  }
  if (print) fprintf(stderr, "max_diag_offset: %d\n", max_diag_offset);

  if (x != N || y != N) {
    if (print) printf("0\nThe path ends in cell (%d, %d) and was expected to end in (%d, %d).\n",
                      x, y, N, N);
    return 0;
  }

  return 1;
}

int main(int argn, char **argv) {
  assert(argn == 4);

  FILE *fin, *fout, *fsol;
  assert( fin = fopen(argv[1], "r"));
  assert(fout = fopen(argv[2], "r"));
  assert(fsol = fopen(argv[3], "r"));

  assert(fscanf(fin, "%d %d %d", &N, &B, &P) == 3);
  assert(N>=MIN || N<=MAX);
  assert(B>=MIN || B<=MAX);
  assert(P>=MIN || P<=MAX);
  assert(B > P && gcd(B, P) == 1);

  ll ans, calculated_ans;
  assert(fscanf(fsol, "%lld", &ans) == 1);
  assert(ans>=0);
  assert(check_solution(fsol, calculated_ans, false));
  ASSERT(ans == calculated_ans, "Expected " << ans << " but got " << calculated_ans);

  ll len, calculated_len;
  if (fscanf(fout, "%lld", &len) != 1) {
    printf("0\nMissing sum of the path cells on the first line.\n");
    return 0;
  }
  if (len != ans) {
    printf("0\nIncorrect max sum %lld which was expected to be %lld.\n", len, ans);
    return 0;
  }
  if (!check_solution(fout, calculated_len, true))
    return 0;
  if (calculated_len != len) {
    printf("0\nThe length of the output path is %lld and it was expected to be %lld.\n",
           calculated_len, ans);
    return 0;
  }

  printf("10\nOK\n");

  return 0;
}
