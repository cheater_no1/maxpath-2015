/*
PROB: given a martix F(1..N, 1..N) find a path with max sum of cells
      going from (1,1) to (N,N) and moving only (x,y) -> (x+1,y) or (x,y) -> (x,y+1)
SOLUTION: dynamic programming
          + forgetting the paths from nodes which are for sure not leading to a best path
          + only using cells near the diagonal (with some small probability of incorrectness)
COMPLEXITY: O(N^2) time and
            O(N^2) memory at worst case
            O(?) memory at average
AUTHOR: Petar Ivanov <peter.ivanov89@gmail.com>
COMMENT: not ready
*/

#include <iostream>
#include <unordered_map>
#include <assert.h>
#include "rng.h"
using namespace std;

enum { UP, LEFT };
const int MAX_N = 10000 + 2;
const int WIDTH = 1000 + 2;

int N, B, P;
ll dp[2][MAX_N];
unordered_map<ll, bool> path;

// stats
int erased=0, max_path_size=0;

inline ll h(int x, int y) {
  return x*MAX_N + y;
}

inline bool& p(int x, int y) {
  return path[h(x,y)];
}

// While (x,y) is guaranteed to not be part of a best path,
// erase (x,y) and recursively go back directing to the root.
void maybe_cut_rec(int curr_row, int x, int y) {
  if (x<1 || y<1) return;
  // assert(x>=1 && y>=1);
  //cerr << "maybe_cut_rec: " << x << " " << y << endl;

  if (x+1>N || !path.count(h(x+1,y)) || p(x+1,y) == LEFT) {
    if(x+1<=N && x+1>curr_row) return;
    if (y+1>N || !path.count(h(x,y+1)) || p(x,y+1) == UP)
      if (x!=N || y!=N) {
        if(path.count(h(x,y))) {
          bool dir = p(x,y);
          //cerr << "erase " << x << " " << y << endl;
          path.erase(h(x,y));
          erased++;
          if (dir == UP) x--;
          else y--;
          maybe_cut_rec(curr_row, x, y);
        }
      }
  }
}

ll solve() {
  ll up, left;
  for (int i=1; i <= N; i++) {
    for (int j=max(1, i-WIDTH); j <= min(N, i+WIDTH); j++) {
      up = dp[(i-1)&1][j], left = dp[i&1][j-1];
      if (up > left) {
        dp[i&1][j] = F(i, j, B, P) + up;
        p(i,j) = UP;
        if(j>1) maybe_cut_rec(i,i,j-1);
      } else {
        dp[i&1][j] = F(i, j, B, P) + left;
        p(i,j) = LEFT;
        if (up != left) {  // possibly more than one best paths
          if(i>1) maybe_cut_rec(i,i-1,j);
        }
      }
      if (max_path_size < (int)path.size())
        max_path_size = (int)path.size();
      //cerr << (p(i,j) == UP ? "^" : "<");
      //cerr << "path.size() = " << path.size() << endl;
    }
    //cerr << endl;
  }

  return dp[N&1][N];
}

void print_path(int x, int y) {
  if (x>1 || y>1) {
    if (path[h(x,y)] == UP)
      print_path(x-1, y);
    else
      print_path(x, y-1);
  }
  cout << x << " " << y << endl;
}

int main() {
  cin >> N >> B >> P;
  cout << solve() << endl;
  print_path(N, N);
  print_memory("diag-cut-branche");
  cerr << "remained = " << path.size() << " which must be >= " << 2*N-1 << endl;
  cerr << "  erased = " << erased << endl;
  cerr << "max_size = " << max_path_size << endl;
  assert(int(path.size()) >= 2*N-1);
  return 0;
}
