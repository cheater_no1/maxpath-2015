/*
PROB: given a martix F(1..N, 1..N) find a path with max sum of cells
      going from (1,1) to (N,N) and moving only (x,y) -> (x+1,y) or (x,y) -> (x,y+1)
SOLUTION: divide and conquere, dynamic programming, the same idea as  maxpath-pesho.cpp
COMPLEXITY: O(N^2) time and O(N) memory
AUTHOR: Iskren Chernev <iskren.chernev@gmail.com>
*/
#include <stdio.h>
#include "rng.h"

const int MAXN = 1 << 16;
typedef long long dp_t;

int N, B, P;

//inline int F(int x, int y) {
//  return ((long long) (x * B + y)) * B % P;
//}

dp_t tmp_arr[MAXN];
dp_t mid_col_a[MAXN];
dp_t mid_col_b[MAXN];
int path[MAXN * 2][2];

dp_t compute(int i, int j, dp_t a, dp_t b) {
  dp_t c = F(i, j, B, P);
  return (a > b ? a : b) + c;
}

void record(int i, int j) {
  int row = i + j - 2;
  path[row][0] = i;
  path[row][1] = j;
}

void solve_a(int x1, int y1, int x2, int y2, dp_t *mid_col) {
  for (int j = y1 - 1; j <= y2; ++j) {
    tmp_arr[j] = 0;
  }

  for (int i = x1; i <= x2; ++i) {
    for (int j = y1; j <= y2; ++j) {
      tmp_arr[j] = compute(i, j, tmp_arr[j-1], tmp_arr[j]);
    }
    mid_col[i] = tmp_arr[y2];
  }
}

void solve_b(int x1, int y1, int x2, int y2, dp_t *mid_col) {
  for (int j = y1; j <= y2 + 1; ++j) {
    tmp_arr[j] = 0;
  }

  for (int i = x2; i >= x1; --i) {
    for (int j = y2; j >= y1; --j) {
      tmp_arr[j] = compute(i, j, tmp_arr[j], tmp_arr[j+1]);
    }
    mid_col[i] = tmp_arr[y1];
  }
}

void solve(int x1, int y1, int x2, int y2) {
  // printf("%d %d %d %d -> ", x1, y1, x2, y2);
  if (x1 == x2) {
    for (int j = y1; j <= y2; ++j) {
      record(x1, j);
    }
    // printf("END\n");
    return;
  }
  if (y1 == y2) {
    for (int i = x1; i <= x2; ++i) {
      record(i, y1);
    }
    // printf("END\n");
    return;
  }

  int y_mid = (y1 + y2) / 2;
  solve_a(x1, y1, x2, y_mid, mid_col_a);
  solve_b(x1, y_mid + 1, x2, y2, mid_col_b);
  dp_t max = -1;
  int x_mid = -1;
  for (int i = x1; i <= x2; ++i) {
    dp_t cand = mid_col_a[i] + mid_col_b[i];
    if (max < cand) {
      max = cand;
      x_mid = i;
    }
  }
  record(x_mid, y_mid);
  record(x_mid, y_mid + 1);
  // printf("%d %d -> %d %d\n", x_mid, y_mid, x_mid, y_mid + 1);
  solve(x1, y1, x_mid, y_mid);
  solve(x_mid, y_mid + 1, x2, y2);
}

int main() {
  assert(scanf("%d %d %d", &N, &B, &P) == 3);
  solve(1, 1, N, N);

  dp_t total = 0;
  for (int i = 0; i < 2 * N - 1; ++i) {
    total += F(path[i][0], path[i][1], B, P);
  }
  printf("%Ld\n", total);
  for (int i = 0; i < 2 * N - 1; ++i) {
    printf("%d %d\n", path[i][0], path[i][1]);
  }
  print_memory("iskren          ");
}
