/*
PROB: given a martix F(1..N, 1..N) find a path with max sum of cells
      going from (1,1) to (N,N) and moving only (x,y) -> (x+1,y) or (x,y) -> (x,y+1)
SOLUTION: dynamic programming
COMPLEXITY: O(N^2) time and O(N^2) memory but storing only 1bit per cell
AUTHOR: Petar Ivanov <peter.ivanov89@gmail.com>
*/

#include <iostream>
#include <cstdio>
#include <vector>
#include <assert.h>

#include "rng.h"
using namespace std;

const int MAX_N = 10000 + 2;

int N, B, P;
ll dp[2][MAX_N];
vector<bool> path[MAX_N];

ll solve() {
  for (int i=1; i <= N; i++)
    for (int j=1; j <= N; j++) {
      if (dp[(i-1)&1][j] > dp[i&1][j-1]) {
        dp[i&1][j] = F(i, j, B, P) + dp[(i-1)&1][j];
        path[i][j] = 0;
      } else {
        dp[i&1][j] = F(i, j, B, P) + dp[i&1][j-1];
        path[i][j] = 1;
      }
    }

  return dp[N&1][N];
}

void print_path(int x, int y) {
  if (x>1 || y>1) {
    if (path[x][y] == 0)
      print_path(x-1, y);
    else
      print_path(x, y-1);
  }
  cout << x << " " << y << endl;
}

int main() {
  cin >> N >> B >> P;
  for(int i=0; i<=N; i++)
    path[i].resize(N+1);
  //print_matrix();
  cout << solve() << endl;
  print_path(N, N);
  print_memory("compressed-dummy");
  return 0;
}
