/*
PROB: given a martix F(1..N, 1..N) find a path with max sum of cells
      going from (1,1) to (N,N) and moving only (x,y) -> (x+1,y) or (x,y) -> (x,y+1)
SOLUTION: N times dynamic programming
COMPLEXITY: O(N^3) time and O(N) memory
AUTHOR: Petar Ivanov <peter.ivanov89@gmail.com>
*/

#include <iostream>
#include <cstdio>
#include <assert.h>
#include <memory.h>

#include "rng.h"
using namespace std;

const int MAX_N = 10000 + 2;

int N, B, P;
ll dp[2][MAX_N];

int x = 1, y = 1;

void solve() {
  memset(dp, 0, 2*MAX_N*sizeof(int));

  for (int i=N; i >= x; i--)
    for (int j=N; j >= y; j--)
      dp[i&1][j] = F(i, j, B, P) + max(dp[i&1][j+1], dp[(i+1)&1][j]);

  if (x == 1 && y == 1)
    cout << dp[x&1][y] << endl;
  cout << x << " " << y << endl;

  if (dp[x&1][y+1] > dp[(x+1)&1][y]) y++;
  else x++;
}

int main() {
  cin >> N >> B >> P;
  if (N > 1000) return 0;
  while(x <= N && y <= N)
    solve();
  print_memory("slow            ");
  return 0;
}
