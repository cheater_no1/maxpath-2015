#!/bin/bash

exe=$1
ext=$2

do_it() {
  g++ -Wfatal-errors -std=c++11 -Wall -O2 -o $exe $exe.cpp
  i_fn=$1
  echo $i_fn
  o_fn=${i_fn%.in}.$ext
  s_fn=${i_fn%.in}.sol
  ./$exe  < $i_fn > $o_fn
  ./checker $i_fn $o_fn $s_fn
}

if [ $# -eq 3 ]; then
  do_it $3
else
  for i_fn in tests/maxpath.*.in; do
    do_it $i_fn
  done
fi
