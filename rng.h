#include <cstdio>
#include <assert.h>
#include <stdlib.h>
#include <sys/resource.h>
#include <errno.h>
using namespace std;

typedef long long ll;

//static const int MAXX = 10000+5;
//static int A[MAXX][MAXX];
//inline int F_(int x, int y, int B, int P) {
//  if (!A[0][0]) {
//    A[0][0]=-1;
//    srand(P);
//    for(int i=1; i<MAXX; i++)
//      for(int j=1; j<MAXX; j++)
//        A[i][j] = rand() % P;
//  }
//  return A[x][y];
//}

inline int F_(long long x, long long y, int B, int P) {
  return ((x*B) ^ ((y+1)*B)) % P;
}
//inline int F_(ll x, ll y, int B, int P) { return (((x * y + B) + (13*x + 11*y)) % P; }
//inline int F_(ll x, ll y, int B, int P) { return (((x * y + B) % 1303 + 7) * (13*x + 11*y)) % P; }
//inline int F_(int x, int y, int B, int P) { return (long long)((x + y) * ((x * y) % 1303 + 7) + 11*x + 13*y) * B % P; }
//inline int F_(int x, int y, int B, int P) { return ((x*B+13)^((x+5)*y+37)) % P; }
//inline int F_(int x, int y, int B, int P) { srand(x*20000+y); return rand()%P; }
//(((x*B+37)*(y+59))^((x+97)*(y*B+71))) % P; }
//inline int F_(int x, int y, int B, int P) { return 1; }

//inline int F_(int x, int y, int B, int P) { return (x * B + y) % P; }
//inline int F_(int x, int y, int B, int P) { return (x * B + y) * B % P; } // not positive!

inline int F(int x, int y, int B, int P) {
  int f = F_(x, y, B, P);
  assert(f >= 0);
  return f;
}

void print_memory(const char *caller) {
  errno = 0;
  struct rusage* m = (rusage*) malloc(sizeof(struct rusage));
  getrusage(RUSAGE_SELF, m);
  if(errno == EFAULT)
      fprintf(stderr, "Error: EFAULT\n");
  else if(errno == EINVAL)
      fprintf(stderr, "Error: EINVAL\n");

  // Memory
  fprintf(stderr, "%s -> %6.2lf MB, ", caller, m->ru_maxrss / 1024.0);

  // Time
  struct rusage tusage;
  tusage.ru_utime.tv_sec =  m->ru_utime.tv_sec + m->ru_stime.tv_sec;
  tusage.ru_utime.tv_usec = m->ru_utime.tv_usec + m->ru_stime.tv_usec;
  tusage.ru_utime.tv_sec += tusage.ru_utime.tv_usec / 1000000;
  tusage.ru_utime.tv_usec = tusage.ru_utime.tv_usec % 1000000;
  fprintf(stderr, "%5.2lf sec\n", tusage.ru_utime.tv_sec+tusage.ru_utime.tv_usec/1000000.0);
}
